<?php

/**
 * Implements hook_feeds_processor_targets_alter().
 */
function commerce_feeds_guid_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  foreach (field_info_instances($entity_type, $bundle_name) as $name => $instance) {
    $info = field_info_field($name);
    if ($info['type'] == 'commerce_product_reference') {
      if (array_key_exists('product_id', $info['columns'])) {
        // Real target is modified in order to keep the older values from the
        // field of the entity to support accumulative imports.
        // See http://drupal.org/node/1188994

        $targets[$name . ":guid"] = array(
          'name' => $instance['label'] . ': GUID',
          'callback' => 'commerce_feeds_guid_feeds_set_target',
          'description' => t('The GUID or its prefix for the @name field. NOTE: the product needs to exist.', array('@name' => $instance['label'])),
          'real_target' => 'commerce-' . $name,
        );
      }
    }
  }
}

/**
 * Callback for mapping. Here is where the actual mapping happens.
 *
 * When the callback is invoked, $target contains the name of the field the
 * user has decided to map to and $value contains the value of the feed item
 * element the user has picked as a source.
 */
function commerce_feeds_guid_feeds_set_target(FeedsSource $source, $entity, $target, $value) {
  if (empty($value)) {
    return;
  }

  // Handle non-multiple value fields.
  if (!is_array($value)) {
    $value = array($value);
  }

  // Get field information.
  list($field_name, $sub_field) = explode(':', $target);
  $info = field_info_field($field_name);
  $field = isset($entity->$field_name) ? $entity->$field_name : array();

  // If we're updating the existing content, we'd want to keep the existing
  // field content and update it accordingly.
  // Also, if the field is empty, we map the values the usual way.
  if ($source->importer->config['processor']['config']['update_existing'] == FEEDS_UPDATE_EXISTING && !empty($field) && count($field[LANGUAGE_NONE])) {
    $i = count($field[LANGUAGE_NONE]);

    foreach ($value as $v) {
      if (!is_array($v) && !is_object($v)) {
        if ($sub_field == 'guid') {
          $product_ids = db_select('feeds_item')
            ->fields('feeds_item', array('entity_id'))
            ->condition('entity_type', 'commerce_product')
            ->condition('guid', db_like($v) . '%', 'LIKE')
            ->execute();

          while ($product_id = $product_ids->fetchField()) {
            $delta = $i;
            // If there are already some elements in the field, we search for it to reuse them.
            foreach($field[LANGUAGE_NONE] as $key => $id) {
              if ($id['product_id'] == $product_id) {
                $delta = $key;
                break;
              }
            }
            $field[LANGUAGE_NONE][$delta]['product_id'] = $product_id;
            $i++;
            if ($info['cardinality'] == 1) {
              break;
            }
          }
          if (empty($product_id)) {
            drupal_set_message(t('A product with GUID starting with %guid could not be found. Please check that the product exists or import it first.', array('%guid' => $v)), 'error');
          }
        }
      }
      if ($info['cardinality'] == 1) {
        break;
      }
    }
  }
  else {
    // If we're not updating, we're replacing or creating so a normal behavior
    // is expected.
    $i = 0;
    foreach ($value as $v) {
      if (!is_array($v) && !is_object($v)) {
        if ($sub_field == 'guid') {
          $product_ids = db_select('feeds_item')
            ->fields('feeds_item', array('entity_id'))
            ->condition('entity_type', 'commerce_product')
            ->condition('guid', db_like($v) . '%', 'LIKE')
            ->execute();
          while ($product_id = $product_ids->fetchField()) {
            $field[LANGUAGE_NONE][$i]['product_id'] = $product_id;
            $i++;
            if ($info['cardinality'] == 1) {
              break;
            }
          }
          if (empty($product_id)) {
            drupal_set_message(t('A product with GUID starting with %guid could not be found. Please check that the product exists or import it first.', array('%guid' => $v)), 'error');
          }
        }
      }
      if ($info['cardinality'] == 1) {
        break;
      }
    }
  }

  $entity->{$field_name} = $field;
}